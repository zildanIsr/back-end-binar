const request = require("supertest");
const app = require("../../../../app");

describe('update car', () => {
    beforeEach(async () => {
        const accessTokenAdmin = await request(app).post("/v1/auth/login").send({
            email: "admin@binar@co.id",
			password: "123456",

        })

        const accessTokenUser = await request(app).post("/v1/auth/login").send({
            email: "fikri@binar.co.id",
			password: "123456",
        })

        const car = await request(app)
            .post("/v1/cars")
            .set("Content-Type", "application/json")
            .set("Authorization", `Bearer ${accessTokenAdmin.body.accessToken}`)
            .send({
                name: "name",
                price: 100,
                size: "small",
                image: "image",
            })

        return car, accessTokenAdmin, accessTokenUser;
    });

    test("should response with 201 as status code", async () => {
        return request(app)
            .put(`/v1/cars/${car.body.id}`)
            .set("Content-Type", "application/json")
            .set("Authorization", `Bearer ${accessTokenAdmin.body.accessToken}`)
            .send({
                name: "name",
                price: 100,
                size: "medium",
                image: "image",
            })
            .then((res) => {
                expect(res.statusCode).toBe(201);
            })
    });

    test("should response with 401 as status code", async () => {
        return request(app)
            .put(`/v1/cars/${car.body.id}`)
            .set("Content-Type", "application/json")
            .set("Authorization", `Bearer ${accessTokenUser.body.accessToken}`)
            .send({
                name: "name",
                price: 100,
                size: "medium",
                image: "image",
            })
            .then((res) => {
                expect(res.statusCode).toBe(401);
            })
    });

    afterEach(async () => {
        return request(app)
            .delete(`/v1/cars/${car.body.id}`)
            .set("Content-Type", "application/json")
            .set("Authorization", `Bearer ${accessTokenAdmin.body.accessToken}`)
    });

});