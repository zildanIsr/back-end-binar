const request = require("supertest");
const app = require("../../../app");

describe("Post /v1/cars", () => {
    const accessToken = await request(app).post("/v1/auth/login").send({
        email: "admin@binar@co.id",
        password: "123456",
    });

    test("should response with 201 as status code", async () => {
        return request(app)
            .post("/v1/cars")
            .set("Content-Type", "application/json")
            .set("Authorization", `Bearer ${accessToken.body.accessToken}`)
            .send({
                name: "name",
                price: 100,
                size: "small",
                image: "image",
            })
            .then((res) => {
                expect(res.statusCode).toBe(201);
            })
    });
});