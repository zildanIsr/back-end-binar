const request = require("supertest");
const app = require("../../../app");

describe("GET /v1/cars", () => {
	test("should response with 200 as status code", async () => {
		return request(app)
			.get("/v1/cars")
			.then((res) => {
				expect(res.statusCode).toBe(200);
			});
	});
});
