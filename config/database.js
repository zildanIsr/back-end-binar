const {
  DB_USER = "",
  DB_PASSWORD = "",
  DB_NAME = "",
  DB_HOST = "",
  DB_PORT = "",
} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}_development`,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: `${DB_NAME}_test`,
    host: DB_HOST,
    port: DB_PORT,
    dialect: "postgres"
  },
  production: {
    username: "pewragmbjwguxw",
    password: "5b35f64d8d6467ab6a2237bd26eb7504cef198ad4f471cdfd6d486e44bfc44de",
    database: "d1m5mmvkjf9trn",
    host: "ec2-3-226-163-72.compute-1.amazonaws.com",
    dialect: "postgres",
    operatorsAliases: false,
    use_env_variable: process.env.DB_URI,
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false
      }
    }
  }
}
