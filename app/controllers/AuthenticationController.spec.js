const AuthenticationController = require("./AuthenticationController");
const { User } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

describe("AuthenticationController", () => {
    describe("#handleLogin", () => {
        it("should return a token", async () => {
            const email = "email@gmail.com";
            const password = "password";
            const roleModel = "ADMIN";

            const mockRequest = {
                body: {
                    email,
                    password,
                },
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            const mockNext = {
                next: jest.fn(),
            }
            
            const mockUser = new User({email, password});
            const mockUserModel = {
                findOne: jest.fn().mockReturnValue(mockUser),
            };

            const authController = new AuthenticationController({userModel : mockUserModel, roleModel , bcrypt, jwt});
            await authController.handleLogin(mockRequest, mockResponse, mockNext);

            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalledWith({
                token: expect.any(String),
                user: {
                    email,
                    role: {
                        name: role,
                    },
                },
            });
        });
    })

    describe('#handleRegister', () => {
        it('should return with code 201', async () => {
            const name = "name";
            const email = "email@gmail.com";
            const password = "password";
            const roleModel = "ADMIN";

            const mockRequest = {
                body: {
                    name,
                    email,
                    password,
                }
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            }

            const mockUser = new User({name, email, password});
            const mockUserModel = {
                create: jest.fn().mockReturnValue(mockUser),
            };

            const authController = new AuthenticationController({userModel : mockUserModel, roleModel , bcrypt, jwt});
            await authController.handleRegister(mockRequest, mockResponse);

            expect(mockResponse.status).toHaveBeenCalledWith(201);
            expect(mockResponse.json).toHaveBeenCalledWith({mockUser});
        });
    });
});