const { Car } = require("../models");
const CarController = require("./CarController");
const dayjs = require("dayjs");

describe('CarController', () => {
    describe('#handleListCars', () => {
        it("should call res.status(200) and res.json with task instance", async () => {
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";
            const isCurrentlyRented = false;
            

            const mockRequest = {
                query : {
                    page : 1,
                    pageSize : 10,
                    pageCount : 0,
                    count : 0,
                },
            };

            const mockCar = new Car({name, price, size, image, isCurrentlyRented});
            const mockCarModel = {};
            mockCarModel.findAll = jest.fn().mockReturnValue(mockCar);

            const mockResponse = {};
            mockResponse.status = jest.fn().mockReturnThis();
            mockResponse.json = jest.fn().mockReturnThis();

            const carController = new CarController({carModel: mockCarModel, userCarModel: {}, dayjs: dayjs});
            await carController.handleListCars(mockRequest, mockResponse);

            expect(mockCarModel.findAll).toHaveBeenCalledWith(1);
            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalledWith(mockCar);

        })
    });

    describe('#handleCreateCar', () => {
        it("should call res.status(201) and res.json with task instance", async () => {
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";

            const mockRequest = {
                body : {
                    name,
                    price,
                    size,
                    image,
                }
            }

            const car = new Car({name, price, size, image});
            const mockCarModel = {create: jest.fn().mockReturnValue(car)};

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            const carController = new CarController({carModel: mockCarModel, userCarModel: {}, dayjs: dayjs});

            await carController.handleCreateCar(mockRequest, mockResponse);

            expect(mockCarModel.create).toHaveBeenCalled();
            expect(mockResponse.status).toHaveBeenCalledWith(201);
            expect(mockResponse.json).toHaveBeenCalledWith(car);
        });

        it("should call res.status(422) and res.json with error", async () => {
            const err = new Error("something");
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";
            const isCurrentlyRented = false;

            const mockRequest = {
                body : {
                    name,
                    price,
                    size,
                    image,                        
                }
            };

            const mockCarModel = {
                create: jest.fn().mockReturnValue(Promise.reject(err))
            };

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis(),
            };

            const carController = new CarController({carModel: mockCarModel, userCarModel: {}, dayjs: dayjs});

            await carController.handleCreateCar(mockRequest, mockResponse);

            expect(mockCarModel.create).toHaveBeenCalledWith({
                name,
                price,
                size,
                image,   
                isCurrentlyRented,             
            });
            expect(mockResponse.status).toHaveBeenCalledWith(422);
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                },
            });
        })
    });

    describe('#getCarFromRequest', () => {
        it("should return a car", async () => {
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";
            const isCurrentlyRented = false;

            const mockRequest = {
                params : {
                    id : 1,
                }
            }

            const mockResponse = {};
            mockResponse.status = jest.fn().mockReturnThis();
            mockResponse.json = jest.fn().mockReturnThis()

            const mockCar = new Car({name, price, size, image, isCurrentlyRented});

            const carController = new CarController({carModel: mockCar, userCarModel: {}, dayjs: dayjs});
            const testing = await carController.handleGetCar(mockRequest, mockResponse);

            expect(mockCar.findByPk).toHaveBeenCalledWith(1);
            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalledWith(testing);
        });
    });

    describe('#handleUpdateCar', () => {
        it("should call res.status(200) and res.json with task instance", async () => {
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";
            const isCurrentlyRented = false;

            const mockRequest = {
                params : {
                    id : 1,
                },
                body : {
                    name,
                    price,
                    size,
                    image,
                    isCurrentlyRented,
                }
            };

            const mockCar = new Car({name, price, size, image, isCurrentlyRented});
            mockCar.update = jest.fn().mockReturnThis();

            const mockCarModel = {};
            mockCarModel.findByPk = jest.fn().mockReturnValue(mockCar);

            const mockResponse = {};
            mockResponse.status = jest.fn().mockReturnThis();
            mockResponse.json = jest.fn().mockReturnThis();

            const carController = new CarController({carModel: mockCarModel, userCarModel: {}, dayjs: dayjs});
            await carController.handleUpdateCar(mockRequest, mockResponse);

            expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
            expect(mockCar.update).toHaveBeenCalledWith({ name, price, size, image, isCurrentlyRented });
            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalledWith(mockCar);
        });
    });

    describe('#handleDeleteCar', () => {
        it("should call res.status(204) and res.json with task instance", async () => {
            const name = "test";
            const price = 100;
            const size = "small";
            const image = "test.jpg";
            const isCurrentlyRented = false;

            const mockRequest = {
                params : {
                    id : 1,
                }
            };

            const mockCar = new Car({name, price, size, image, isCurrentlyRented});
            mockCar.destroy = jest.fn();

            const mockCarModel = {};
            mockCarModel.findByPk = jest.fn().mockReturnValue(mockCar);

            const mockResponse = {};
            mockResponse.status = jest.fn().mockReturnThis();
            mockResponse.end = jest.fn().mockReturnThis();

            const carController = new CarController({carModel: mockCarModel, userCarModel: {}, dayjs: dayjs});
            await carController.handleDeleteCar(mockRequest, mockResponse);

            expect(mockCarModel.findByPk).toHaveBeenCalledWith(1);
            expect(mockCar.destroy).toHaveBeenCalled();
            expect(mockResponse.status).toHaveBeenCalledWith(204);
            expect(mockResponse.end).toHaveBeenCalled();
        });
    });
})